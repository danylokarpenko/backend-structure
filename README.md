Backend side of application has microservice architecture. This will help to build and maintain useful, high quality and scalable software.
The microservice software architecture allows a system to be divided into a number of smaller, individual and independent services. Each service is flexible, robust, composable and complete. They run as autonomous processes and communicate with one another through APIs. Each microservice can be implemented in a different programming language on a different platform.

BENEFITS OF MICROSERVICES:
* Easier to Build and Maintain Apps
* Organized Around Business Capabilities
* Improved Productivity and Speed
* Flexibility in Using Technologies and Scalability
* Autonomous, Cross-functional Teams

In this application we use HTTP protocols to interact with client.

Deeper into implementation:

* Create a file in the root folder for your project called server.js. This file creates our server and assigns routes to process all requests.